from isiguardianlib import const as top_const

########################################
# Sensor Correction

# NOTE: The colum with DEFAULT is the name of the configuration
# The filter bank names should go into the next colum.

SC_FM_confs = {
    'ETMX' : {
        'BSC_ST1' : {
            'DEFAULT' : { # Only the FMs that do no change and are always on.
                # All of the banks used should be in this level, if no FMs
                # engaged is the default, then please put the bank in with
                # empty FM lists.
                # Keep dofs consistent.
                # If no FMs engaged, but bank should have gain, place a 0
                #FIXME: add the 0 arg to the fm switcher
                'WNR' : {
                    'X' : [9],
                    'Y' : [9],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'FIR' : {
                    'X' : [5],
                    'Y' : [7],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'IIRHP' : {
                    'X' : [5],
                    'Y' : [5],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},            
            'WNR' : { # useism
                'WNR' : {
                    'X' : [9], 
                    'Y' : [9],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'FIR' : { # Windy
                'FIR' : {
                    'X' : [5],
                    'Y' : [7],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'IIRHP' : { # EQ
                'IIRHP' : {
                    'X' : [5],
                    'Y' : [5],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},},
        'BSC_ST2' : {
            'DEFAULT' : { # Only the FMs that do no change and are always on.
                # All of the banks used should be in this level, if no FMs
                # engaged is the default, then please put the bank in with
                # empty FM lists.
                'WNR' : {
                    'X' : [],
                    'Y' : [],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'FIR' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'IIRHP' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},                        
            'FIR' : { # useism
                'FIR' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'IIRHP' : { 
                'IIRHP' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},},},
    'ETMY' : {
        'BSC_ST1' : {
            'DEFAULT' : { # Only the FMs that do no change and are always on.
                # All of the banks used should be in this level, if no FMs
                # engaged is the default, then please put the bank in with
                # empty FM lists.
                'WNR' : {
                    'X' : [7],
                    'Y' : [7],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'FIR' : {
                    'X' : [10],
                    'Y' : [10],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'IIRHP' : {
                    'X' : [1],
                    'Y' : [1],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},            
            'WNR' : { # Useism 
                'WNR' : {
                    'X' : [7],
                    'Y' : [7],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'FIR' : { # Windy
                'FIR' : {
                    'X' : [10],
                    'Y' : [10],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'IIRHP' : { # EQ
                'IIRHP' : {
                    'X' : [1],
                    'Y' : [1],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},},
        'BSC_ST2' : {
            'DEFAULT' : { # Only the FMs that do no change and are always on.
                # All of the banks used should be in this level, if no FMs
                # engaged is the default, then please put the bank in with
                # empty FM lists.
                'WNR' : {
                    'X' : [],
                    'Y' : [],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'FIR' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'IIRHP' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},            
            'FIR' : {  
                'FIR' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'IIRHP' : { 
                'IIRHP' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},},},
    'ITMX' : {
        'BSC_ST1' : {
            'DEFAULT' : { # Only the FMs that do no change and are always on.
                # All of the banks used should be in this level, if no FMs
                # engaged is the default, then please put the bank in with
                # empty FM lists.
                'WNR' : {
                    'X' : [10],
                    'Y' : [10],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'FIR' : {
                    'X' : [5],
                    'Y' : [5],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'IIRHP' : {
                    'X' : [6],
                    'Y' : [6],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},            
            'WNR' : { # Useism 
                'WNR' : {
                    'X' : [10],
                    'Y' : [10],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'IIRHP' : { # Useism 
                'IIRHP' : {
                    'X' : [6],
                    'Y' : [6],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},            
            'FIR' : { # Windy
                'FIR' : {
                    'X' : [5],
                    'Y' : [5],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},},
        'BSC_ST2' : {
            'DEFAULT' : { # Only the FMs that do no change and are always on.
                # All of the banks used should be in this level, if no FMs
                # engaged is the default, then please put the bank in with
                # empty FM lists.
                'WNR' : {
                    'X' : [],
                    'Y' : [],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'FIR' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'IIRHP' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},                        
            'FIR' : { 
                'FIR' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'IIRHP' : { 
                'IIRHP' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},},},
    'ITMY' : {
        'BSC_ST1' : {
            'DEFAULT' : { # Only the FMs that do no change and are always on.
                # All of the banks used should be in this level, if no FMs
                # engaged is the default, then please put the bank in with
                # empty FM lists.
                'WNR' : {
                    'X' : [10],
                    'Y' : [10],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'FIR' : {
                    'X' : [5],
                    'Y' : [5],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'IIRHP' : {
                    'X' : [6],
                    'Y' : [6],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},            
            'WNR' : { # Useism 
                'WNR' : {
                    'X' : [10],
                    'Y' : [10],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'IIRHP' : { # Useism 
                'IIRHP' : {
                    'X' : [6],
                    'Y' : [6],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},            
            'FIR' : { # Windy
                'FIR' : {
                    'X' : [5],
                    'Y' : [5],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},},
        'BSC_ST2' : {
            'DEFAULT' : { # Only the FMs that do no change and are always on.
                # All of the banks used should be in this level, if no FMs
                # engaged is the default, then please put the bank in with
                # empty FM lists.
                'WNR' : {
                    'X' : [],
                    'Y' : [],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'FIR' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'IIRHP' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},            
            'FIR' : { 
                'FIR' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'IIRHP' : {
                'IIRHP' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},},},
    'BS' : {
        'BSC_ST1' : {
            'DEFAULT' : { # Only the FMs that do no change and are always on.
                # All of the banks used should be in this level, if no FMs
                # engaged is the default, then please put the bank in with
                # empty FM lists.
                'WNR' : {
                    'X' : [6],
                    'Y' : [6],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'FIR' : {
                    'X' : [10],
                    'Y' : [10],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'IIRHP' : {
                    'X' : [6],
                    'Y' : [6],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},                        
            'WNR' : { # Useism 
                'WNR' : {
                    'X' : [6],
                    'Y' : [6],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'IIRHP' : { # Useism 
                'IIRHP' : {
                    'X' : [6],
                    'Y' : [6],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},            
            'FIR' : { # Windy
                'FIR' : {
                    'X' : [10],
                    'Y' : [10],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},},
        'BSC_ST2' : {
            'DEFAULT' : { # Only the FMs that do no change and are always on.
                # All of the banks used should be in this level, if no FMs
                # engaged is the default, then please put the bank in with
                # empty FM lists.
                'WNR' : {
                    'X' : [],
                    'Y' : [],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'FIR' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},
                'IIRHP' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},            
            'FIR' : { 
                'FIR' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},
            'IIRHP' : {
                'IIRHP' : {
                    'X' : [4],
                    'Y' : [4],
                    'Z' : [],
                    'RX' : [],
                    'RY' : [],
                    'RZ' : []},},},},
    'HAM2': {
        'HAM': {
            'DEFAULT': {
                'FIR': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},
                'IIRHP': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},
                'WNR': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'WNR': {
                'WNR': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'IIRHP': {
                'IIRHP': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'FIR': {
                'FIR': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},}},
    'HAM3': {
        'HAM': {
            'DEFAULT': {
                'FIR': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},
                'IIRHP': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},
                'WNR': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'WNR': {
                'WNR': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'IIRHP': {
                'IIRHP': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'FIR': {
                'FIR': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},}},
    'HAM4': {
        'HAM': {
            'DEFAULT': {
                'FIR': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},
                'IIRHP': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},
                'WNR': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'WNR': {
                'WNR': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'IIRHP': {
                'IIRHP': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'FIR': {
                'FIR': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},}},
    'HAM5': {
        'HAM': {
            'DEFAULT': {
                'FIR': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},
                'IIRHP': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},
                'WNR': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'WNR': {
                'WNR': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'IIRHP': {
                'IIRHP': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'FIR': {
                'FIR': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},}},
    'HAM6': {
        'HAM': {
            'DEFAULT': {
                'FIR': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},
                'IIRHP': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},
                'WNR': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'WNR': {
                'WNR': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'IIRHP': {
                'IIRHP': {
                    'X': [6],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},
            'FIR': {
                'FIR': {
                    'X': [10],
                    'Y': [1],
                    'Z': [],
                    'RX': [],
                    'RY': [],
                    'RZ': [],},},}},
}

########################################
SC_FM_confs = SC_FM_confs[top_const.CHAMBER][top_const.CHAMBER_TYPE]
